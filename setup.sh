if which npm 2>/dev/null; then
  echo "npm exists!"
else
  echo "please install node"
  exit
fi
npm i
if which bower 2>/dev/null; then
  echo "bower exists!"
else
  sudo npm i bower -g
fi
cd public && bower i
cd ..
echo "\n\nURL：http://127.0.0.1:3000/demo"
npm start


define(function(require, exports, module) {
    var feedController = require('controllers/feed_list');
    var ratingController = require('controllers/rating');
    var myapp = angular.module('myapp', ["highcharts-ng", "ui.router"]);
    myapp.filter('attituteIcon', function() {
        return function(input) {
            var dict = ['arrow-right right','arrow-up up','arrow-down down'];
            return dict[input];
        };
    })
    myapp.config(function($stateProvider, $urlRouterProvider) {
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/start_page");
        var curStock = {
            code: "QIHU",
            name: "奇虎360"
        }
        // Now set up the states
        $stateProvider
            .state('start_page', {
                url: "/start_page",
                templateUrl: "partials/start_page.html",
                controller: function($scope, $state, $stateParams) {
                    $scope.goToDetail = function() {
                        $state.go('stock_detail.stock_info', {
                            code: $scope.stockCode
                        })
                    }
                }
            })
            .state('stock_detail', {
                url: "/stock_detail",
                templateUrl: "partials/main_view.html",
                controller: function($scope, $stateParams, $state) {
                    
                    $scope.stock = curStock;
                    $scope.cur = $state.current.name;
                    $scope.setCur = function(name) {
                        $scope.cur = name
                    }
                }
            })
            .state('stock_detail.stock_info', {
                url: "/stock_info/:code",
                templateUrl: "partials/stock_info.html",
                controller: function($scope, $stateParams) {
                    curStock.code = $stateParams.code;
                    $scope.stock = curStock;
                    $scope.chartConfig = {
                        options: {
                            chart: {
                                type: 'areaspline'
                            },
                            yAxis: [{
                                title: {
                                    text: '价格'
                                }
                            }, {
                                title: {
                                    text: '成交量'
                                },
                                opposite: true
                            }]
                        },

                        series: [{
                            name: "价格",
                            data: [10, 15, 12, 8, 7, 15, 12, 8, 7, 15, 12, 8, 7]
                        }, {
                            name: "成交量",
                            yAxis: 1,
                            data: [1599, 1265, 8775, 67, 15767, 1256, 8567, 7676, 615, 1662, 7678, 7575, 8567]
                        }],
                        title: {
                            text: '近3个月价格和成交量'
                        },

                        loading: false
                    }
                }
            })
            .state('stock_detail.feed_list', {
                url: "/feed_list/:code",
                templateUrl: "partials/feed_list.html",
                controller: feedController
            })
            .state('stock_detail.rating', {
                url: "/rating/:code",
                templateUrl: "partials/rating.html",
                controller: ratingController
            })
    });


    module.exports = {
        init: function(select, cfg) {
            angular.bootstrap($("body")[0], ['myapp']);
        }
    };

});
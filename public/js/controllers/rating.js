define(function(require, exports, module) {
    module.exports = function($scope, $stateParams) {
        $scope.code = $stateParams.code;
        $scope.feeds = [];
        $.get('data/feed.json',function(data){
            $scope.feeds = $scope.feeds.concat(data);
            $scope.$apply();
        },"json");
        
        $scope.showMore = function() {
            $.get('data/feed.json',function(data){
                $scope.feeds = $scope.feeds.concat(data);
                $scope.$apply();
            },"json");
        }
    };

});